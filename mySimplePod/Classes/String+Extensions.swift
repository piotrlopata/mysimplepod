//
//  String+Extensions.swift
//  mySimplePod
//
//  Created by Piotr Łopata on 25.06.2018.
//

import Foundation

extension String {
    func firstCharacterCapitalized() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
}
