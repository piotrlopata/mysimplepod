# mySimplePod

[![Version](https://img.shields.io/cocoapods/v/mySimplePod.svg?style=flat)](https://cocoapods.org/pods/mySimplePod)
[![License](https://img.shields.io/cocoapods/l/mySimplePod.svg?style=flat)](https://cocoapods.org/pods/mySimplePod)
[![Platform](https://img.shields.io/cocoapods/p/mySimplePod.svg?style=flat)](https://cocoapods.org/pods/mySimplePod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

mySimplePod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'mySimplePod'
```

## Author

pjjotr, piotr.lopata@appchance.com

## License

mySimplePod is available under the MIT license. See the LICENSE file for more info.
